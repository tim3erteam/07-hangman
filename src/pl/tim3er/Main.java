package pl.tim3er;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<String> userdLetter = new ArrayList<>();
        System.out.print("put some password: ");
        String haslo = scanner.nextLine();
        for ( int i = 0; i < 100; i++ ) {
            System.out.println();
        }
        String[] letters = new String[haslo.length()];
        for ( int i = 0; i < letters.length; i++ ) {
            letters[i] = "_";
        }
        int bledy = 0;
        boolean isEnd = false;
        boolean win = false;
        boolean isExtend = false;
        while ( !isEnd && bledy < 7 && !win ) {
            System.out.println(Hangman.staticHANGMANPICS[bledy]);
            if ( bledy < 6 ) {
                for ( int i = 0; i < letters.length; i++ ) {
                    System.out.print(letters[i]);
                }
                System.out.println("put some letter\n you used:\n");
                System.out.println(Arrays.toString(userdLetter.toArray()));
                String znak = scanner.nextLine();
                if ( !userdLetter.contains(znak) ) {
                    userdLetter.add(znak);
                }
                for ( int i = 0; i < haslo.length(); i++ ) {
                    if ( haslo.charAt(i) == znak.charAt(0) ) {
                        letters[i] = znak;
                        isExtend = true;
                    }
                }
                if ( !isExtend ) {
                    bledy++;
                }
                win = true;
                for ( int i = 0; i < letters.length; i++ ) {
                    if ( letters[i] == "_" ) {
                        win = false;
                    }
                }
                isExtend = false;
            }
            if ( bledy == 6 ) {
                isEnd = true;

            }

        }
        if ( win ) {
            System.out.println("you win");
        } else {
            System.out.println(Hangman.staticHANGMANPICS[6]);
            System.out.println("you lose");
        }

    }
}
